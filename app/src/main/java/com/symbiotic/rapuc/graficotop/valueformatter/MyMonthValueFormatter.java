package com.symbiotic.rapuc.graficotop.valueformatter;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DateFormat;
import java.util.Date;

public class MyMonthValueFormatter implements IAxisValueFormatter {

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        //value = (int) Math.ceil(value);

        if (value <1){
            return "Ene.";
        } else if (value<2){
            return "Feb.";
        } else if (value<3){
            return "Mar.";
        } else if (value<4){
            return "Abr.";
        } else if (value<5){
            return "May.";
        } else if (value<6){
            return "Jun.";
        } else if(value<7){
            return "Jul.";
        } else if(value<8){
            return "Ago.";
        } else if(value<9){
            return "Set.";
        } else if(value<10){
            return "Oct.";
        } else if(value<11){
            return "Nov.";
        } else if(value<12){
            return "Dic.";
        } else{
            return null;
        }
    }
}
