package com.symbiotic.rapuc.graficotop.valueformatter;


import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

public class MyWeekValueFormatter implements IAxisValueFormatter {

    public MyWeekValueFormatter() {

    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        //value = (int) Math.ceil(value);
        if (value <1){
            return "Lunes";
        } else if (value<2){
            return "Martes";
        } else if (value<3){
            return "Miércoles";
        } else if (value<4){
            return "Jueves";
        } else if (value<5){
            return "Viernes";
        } else if (value<6){
            return "Sábado";
        } else if(value<7){
            return "Domingo";
        } else{
            return "Fofi";
        }
    }
}