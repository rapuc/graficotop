package com.symbiotic.rapuc.graficotop;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public class Transaction {

    private int  id;

    private double value;

    private String currency;

    private DateTime  date;

    public Transaction(int id, double value, String currency, DateTime  date) {
        this.id = id;
        this.value = value;
        this.currency = currency;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public DateTime  getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public static List<Transaction> getData(){
        DateTime date = new DateTime ("2018-09-22T09:00:00.000-06:00");
        DateTime dateOtheryear = new DateTime ("2019-09-22T09:00:00.000-06:00");
        DateTime dateOtherWeek = new DateTime ("2018-11-22T09:00:00.000-06:00");
        List<Transaction> data = new ArrayList<>();
        double ammount = 2000d;
        int id = 1;
        data.add(new Transaction(id++,id*ammount,"CRC", date));
        data.add(new Transaction(id++,id*ammount,"USD", date));
        date = date.minusDays(1);
        data.add(new Transaction(id++,id*ammount,"CRC", date));
        data.add(new Transaction(id++,id*ammount,"USD", date));

        date = date.minusDays(1);
        data.add(new Transaction(id++,id*ammount,"CRC", date));
        data.add(new Transaction(id++,id*ammount,"USD", date));

        date = date.minusDays(1);
        data.add(new Transaction(id++,id*ammount,"CRC", date));
        data.add(new Transaction(id++,id*ammount,"USD", date));

        date = date.minusDays(1);
        data.add(new Transaction(id++,id*ammount,"CRC", date));
        data.add(new Transaction(id++,id*ammount,"USD", date));

        date = date.minusDays(1);
        data.add(new Transaction(id++,id*ammount,"CRC", date));
        data.add(new Transaction(id++,id*ammount,"USD", date));

        date = date.minusDays(1);
        data.add(new Transaction(id++,id*ammount,"CRC", date));
        data.add(new Transaction(id,id*ammount,"USD", date));

        data.add(new Transaction(id++,id*ammount*1000,"CRC", dateOtherWeek));
        data.add(new Transaction(id,id*ammount*1000,"USD", dateOtheryear));
        return data;


    }
}
