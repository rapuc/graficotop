package com.symbiotic.rapuc.graficotop;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RadioButton;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.symbiotic.rapuc.graficotop.valueformatter.MyMonthValueFormatter;
import com.symbiotic.rapuc.graficotop.valueformatter.MyWeekValueFormatter;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadableDateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<Transaction> dataObjects;
    List<BarEntry> entriesColones;
    List<BarEntry> entriesDolares;

    BarDataSet set1;
    BarDataSet set2;
    double[][] results;
    RadioButton week;
    RadioButton month;
    RadioButton sum;
    RadioButton count;
    BarChart chart;
    XAxis xAxis;

    DateTime now;
    DateTime lastWeek;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        JodaTimeAndroid.init(this);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setVisibility(View.INVISIBLE);

        dataObjects = Transaction.getData();

        week = findViewById(R.id.radio_week);
        month = findViewById(R.id.radio_month);
        sum = findViewById(R.id.radio_sum);
        count = findViewById(R.id.radio_count);
        //week.setChecked(true);
        week.toggle();
        week.setChecked(true);
        sum.toggle();
        sum.setChecked(true);

        //Chart
        chart = findViewById(R.id.chart);
        chart.setDrawBarShadow(false);
        chart.setDrawGridBackground(true);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(true);
        l.setYOffset(0f);
        l.setXOffset(10f);
        l.setYEntrySpace(0f);
        l.setTextSize(10f);


        xAxis = chart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setCenterAxisLabels(true);
        xAxis.setDrawGridLines(false);
        xAxis.setValueFormatter((value, axis) -> String.valueOf((int) value));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(35f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        chart.getAxisRight().setEnabled(false);
        lastWeek = new DateTime(DateTimeZone.forID("America/Costa_Rica")).withMillisOfSecond(0).minusWeeks(1);
        now = new DateTime(DateTimeZone.forID("America/Costa_Rica")).withMillisOfSecond(0).minusWeeks(1);
        refreshData();

        }
        private void refreshData(){
            int entriesAmount;
            if (week.isChecked() ) {
                if (sum.isChecked()) {
                    results = transactionByWeek(true);
                } else { //if (count.isChecked())
                    results = transactionByWeek(false);
                }
                xAxis.setValueFormatter(new MyWeekValueFormatter());
                entriesAmount = 7;

            } else { //month
                if (sum.isChecked()) {
                    results = transactionByMonth(true);
                } else { //if (count.isChecked())
                    results = transactionByMonth(false);
                }
                xAxis = chart.getXAxis();
                xAxis.setValueFormatter(new MyMonthValueFormatter());
                entriesAmount = 12;
            }
            xAxis.setAxisMaximum(entriesAmount);
            Log.d("results", Arrays.toString(results[0]));
            Log.d("results", Arrays.toString(results[1]));
            entriesColones = new ArrayList<>();
            entriesDolares = new ArrayList<>();
            for (int i = 0; i < entriesAmount; i++) {
                entriesColones.add(new BarEntry(i + 0.1f, (float) results[0][i]));
                entriesDolares.add(new BarEntry(i + 0.5f, (float) results[1][i]));
            }
            // create 2 datasets
            set1 = new BarDataSet(entriesColones, "Colones");
            set1.setColor(Color.BLUE);
            set2 = new BarDataSet(entriesDolares, "Dolares");
            set2.setColor(Color.RED);
            BarData data = new BarData(set1, set2);
            data.setBarWidth(0.4f);
            data.setValueTextSize(10f);
            data.setDrawValues(false);

            chart.setData(data);
            chart.invalidate();
            //chart.notifyDataSetChanged();


        }


    private double[][] transactionByWeek(boolean isSum){
        double[][] results = new double[2][7];
        results[0] = new double[7];//colones
        results[1] = new double[7];//dolares
        Arrays.fill(results[0],0);
        Arrays.fill(results[1],0);

        double value = 1; //caso count
        for(Transaction t : dataObjects){
            if(t.getDate().compareTo(lastWeek)<0) {//es de los ultimos 7 dias
                if (isSum)
                    value = t.getValue();

                if (t.getCurrency().equals("CRC"))
                    results[0][t.getDate().getDayOfWeek() - 1] += value;
                else
                    results[1][t.getDate().getDayOfWeek() - 1] += value;
            }
        }
        return  results;
    }


    private double[][] transactionByMonth(boolean isSum){
        double[][] results = new double[2][12];
        results[0] = new double[12];//colones
        results[1] = new double[12];//dolares
        Arrays.fill(results[0],0);
        Arrays.fill(results[1],0);

        double value = 1; //caso count

        for(Transaction t : dataObjects){
            if(t.getDate().getYear()==now.getYear()) {//esta en el año pasado
                if (isSum) {
                    value = t.getValue();
                }

                if (t.getCurrency().equals("CRC"))
                    results[0][t.getDate().getMonthOfYear() - 1] += value;
                else
                    results[1][t.getDate().getMonthOfYear() - 1] += value;
            }
        }
        return  results;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_week:
                if(month.isChecked())
                    month.setChecked(false);
                refreshData();
                break;
            case R.id.radio_month:
                if(week.isChecked())
                    week.setChecked(false);
                refreshData();
                break;
            case R.id.radio_count:
                if(sum.isChecked())
                    sum.setChecked(false);
                refreshData();
                break;
            case R.id.radio_sum:
                if(count.isChecked())
                    count.setChecked(false);
                refreshData();
                break;
        }
    }
}
